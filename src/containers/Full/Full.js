import React, {Component} from 'react';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Dashboard from '../../views/Dashboard/';
import UsersManagement from '../../modules/base/Users/Mnagement/UsersList/';
import UsersCreate from '../../modules/base/Users/Mnagement/UsersCreate/';
import RightsManagement from "../../modules/base/Rigths/RightsManagement/";

import ProfileMain from "../../modules/base/Users/Profile/ProfileMain";

import MyCourses from '../../modules/project/MyCourses/components/MyCourses/';
import CourseView from "../../modules/project/MyCourses/components/CourseView";
import CourseLessonView from "../../modules/project/MyCourses/components/CourseLessonView";

import ErrorPage from "../../modules/base/components/ErrorPage/";

class Full extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="app">
                <Header/>
                <div className="app-body">
                    <Sidebar {...this.props}/>
                    <main className="main">
                        <Breadcrumb/>
                        <Container fluid>
                            <Switch>
                                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>

                                <Route path="/right/management" name="RightsManagement" component={RightsManagement}/>

                                <Route path="/user/management" name="UsersManagement" component={UsersManagement}/>
                                <Route path="/user/create" name="UsersCreate" component={UsersCreate}/>


                                <Route path="/user/profile" name="ProfileMain" component={ProfileMain}/>

                                <Route path="/mycourses" name="MyCourses" component={MyCourses}/>
                                <Route path="/course/:code" component={CourseView}/>
                                <Route path="/lesson/:code" component={CourseLessonView}/>


                                <Redirect from="/" to="/dashboard"/>

                                <Route path="*" component={ErrorPage}/>

                            </Switch>
                        </Container>
                    </main>
                    <Aside/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Full;
