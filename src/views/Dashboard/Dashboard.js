import React, {Component} from 'react';
import { connect } from 'react-redux'


import {
    Row,
    Col,
    Card,
    Progress,
    CardBody,
    CardHeader,
    Button,
} from 'reactstrap';


import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css'
import {CATEGORIES} from "../../constants/routers";


import ListBox from "../../modules/base/components/theme/ListBox";


class SliderComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            volume: this.props.volume,
            homePercent: 100 - this.props.volume,
            guestPercent: this.props.volume,
        };

        this.handleOnChange = this.handleOnChange.bind(this);

    }

    handleOnChange(value) {
        this.setState({
            volume: value,
            homePercent: 100 - value,
            guestPercent: value,
        })
    }

    render() {
        let {volume} = this.state

        return (
            <div>
                <Progress multi color="success">
                    <Progress bar color="default" value={this.state.volume} min={this.props.min}
                              max={this.props.max}>{this.state.volume} %
                    </Progress>
                </Progress>
                <Slider
                    min={this.props.min}
                    max={this.props.max}
                    step={this.props.step}
                    value={volume}
                    onChange={this.handleOnChange}
                />
                <Row>
                    <Col sm="6">
                        <strong className="text-muted">{/*{translate.slider.guest}*/} <small className="text-muted">{this.state.guestPercent} %</small></strong>
                    </Col>
                    <Col sm="6" className="text-right">
                        <strong className="text-success">{/*{translate.slider.home}*/} <small className="text-muted">{this.state.homePercent} %</small></strong>
                    </Col>
                </Row>
            </div>
        )
    }
}

/*

const ages = function getAge(start, end, step) {
    let ages = [];

    for (let i = start; i < end; i += step) {
        ages.push({value: i, name: i})
    }
    return ages
}
*/


import t from '../../modules/base/components/multilang/MultiLang/MultiLangT'

class Dashboard extends Component {

    render() {
        return (
            <div className="animated fadeIn">

                <Card className="card-accent-primary">

                    <CardBody>

                        <Row>
                            <Col xs="12" sm="6" lg="3">
                                <Card className="text-white bg-primary">
                                    <CardBody className="pb-0">
                                        <h4 className="mb-0"><i className="fa fa-users"/> 139</h4>
                                        <p>{t(this).countConnectedUsers}</p>
                                    </CardBody>
                                </Card>
                            </Col>

                            <Col xs="12" sm="6" lg="3">

                                <Card className="text-white bg-primary">
                                    <CardBody className="pb-0">
                                        <h4 className="mb-0"><i className="fa fa-crosshairs"/> 234 287</h4>
                                        <p>Количество заработанных токенов</p>
                                    </CardBody>
                                </Card>
                            </Col>
                            <Col xs="12" sm="6" lg="3">
                                <Card className="text-white bg-primary">
                                    <CardBody className="pb-0">
                                        <h4 className="mb-0"><i className="fa fa-credit-card"/> 2123.80</h4>
                                        <p>Доход по рефералам</p>
                                    </CardBody>
                                </Card>

                            </Col>

                            <Col xs="12" sm="6" lg="3">
                                <Card className="text-white bg-info">
                                    <CardBody className="pb-0">
                                        <h4 className="mb-0"><i className="fa fa-credit-card"/> 9023.80</h4>
                                        Баланс

                                        <Button type="submit" color="success" className="pull-right">
                                            <i className="fa fa-dollar"/> Вывести средства
                                        </Button>

                                    </CardBody>
                                </Card>
                            </Col>



                        </Row>

                        <Row>
                            <Col xs="12" lg="6">

                                <h3>Количество подключений</h3>

                                <div className="area-chart-wrapper" style={{width: '100%', height: '400px'}}>

                                </div>
                                <hr />

                                <h3>Количество показов</h3>

                                <div className="area-chart-wrapper" style={{width: '100%', height: '400px'}}>

                                </div>

                            </Col>
                            <Col xs="12" lg="6">

                                <Card>
                                    <CardHeader>
                                        <i className="fa fa-signal"/> {t(this).channel.distribution}
                                    </CardHeader>
                                    <CardBody>
                                        <SliderComponent
                                            min={0}
                                            max={100}
                                            step={1}
                                            volume={5}
                                        />
                                    </CardBody>
                                </Card>

                               {/* <Card>
                                    <CardHeader>
                                        Аудитория
                                    </CardHeader>
                                    <CardBody>
                                        <Row>
                                            <Col lg="6">
                                                <Row>
                                                    <Col lg="4">
                                                        <label><strong>Пол</strong></label>
                                                    </Col>
                                                    <Col lg="8">
                                                        <Row>
                                                            <Col lg="6">
                                                                <CheckBoxList name="М" checked={true}/>
                                                            </Col>
                                                            <Col lg="6">
                                                                <CheckBoxList name="Ж" checked={true}/>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                                <hr/>
                                                <Row>
                                                    <Col lg="4">
                                                        <label><strong>Возраст</strong></label>
                                                    </Col>
                                                    <Col lg="8">
                                                        <Row>
                                                            <Col lg="6">
                                                                <Select floatingLabelText="с" value={18}
                                                                        items={ages(12, 80, 2)}/>
                                                            </Col>
                                                            <Col lg="6">
                                                                <Select floatingLabelText="по" value={50}
                                                                        items={ages(12, 80, 2)}/>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
*/}
                                <Card>
                                    <CardHeader className="bg-success">
                                        <i className="fa fa-align-justify"></i>
                                        <strong>Предпочтительные интересы</strong>
                                        <small> + </small>
                                    </CardHeader>
                                    <CardBody>
                                        <ListBox items={CATEGORIES}/>
                                    </CardBody>
                                </Card>

                                <Card>
                                    <CardHeader className="bg-danger">
                                        <i className="fa fa-align-justify"></i>
                                        <strong>Запрещенные интересы</strong>
                                        <small> - </small>
                                    </CardHeader>
                                    <CardBody>
                                        <ListBox items={CATEGORIES}/>
                                    </CardBody>
                                </Card>
                            </Col>


                        </Row>


                    </CardBody>
                </Card>


            </div>
        )
    }
}


const mapStateToProps = state => {//state => ({ lang: state.lang })
    return {
        lang: state.lang
    }
}


export default  connect(mapStateToProps)(Dashboard);
