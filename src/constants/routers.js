
export const ROUTERS = [
    {id: 1, name: "ТЕстовая точка 1", created_at: "2013/01/01", status: 1},
    {id: 2, name: "Точка доступа 2", created_at: "2012/06/01", status: 0},
    {id: 3, name: "Точка доступа 3", created_at: "2012/03/01", status: 0},
    {id: 4, name: "Точка 5", created_at: "2012/01/01", status: 1}
]


export const CATEGORIES = [
    {id: 1, name: "Рестораны", created_at: "2013/01/01", status: 1},
    {id: 2, name: "Торговые центры", created_at: "2012/06/01", status: 0},
    {id: 3, name: "Супермаркеты", created_at: "2012/06/01", status: 0},
    {id: 4, name: "Автобусные остановки", created_at: "2012/06/01", status: 0},
    {id: 5, name: "Клубы", created_at: "2012/06/01", status: 0},
    {id: 6, name: "Кафе ", created_at: "2012/06/01", status: 0},
    {id: 7, name: "Гостиницы", created_at: "2012/06/01", status: 0},
    {id: 8, name: "Отели", created_at: "2012/03/01", status: 0},
    {id: 9, name: "Хостелы", created_at: "2012/01/01", status: 1},
    {id: 10, name: "Cалоны красоты / Парикмахерские", created_at: "2012/01/01", status: 1},
    {id: 11, name: "Фитнес клуб", created_at: "2012/01/01", status: 1},
    {id: 12, name: "lounge bar", created_at: "2012/01/01", status: 1},
    {id: 13, name: "Антикафе", created_at: "2012/01/01", status: 1},
    {id: 14, name: "Развлекательный центр", created_at: "2012/01/01", status: 1},
]

export default ROUTERS;