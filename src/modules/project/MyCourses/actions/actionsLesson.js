export function viewHasErrored(bool) {
    return {
        type:           'COURSES_LESSON_VIEW_HAS_ERRORED',
        viewHasErrored: bool
    };
}

export function viewIsLoading(bool) {
    return {
        type:          'COURSES_LESSON_VIEW_IS_LOADING',
        viewIsLoading: bool
    };
}


export function viewFetchDataSuccess(view) {
    return {
        type: 'COURSES_LESSON_VIEW_FETCH_DATA_SUCCESS',
        view: view
    };
}

export function viewFetchData(code, url) {
    return dispatch => {
        dispatch(viewIsLoading(true));

        fetch(new Request(url + '/course/lesson-user-link/view?code=' + code + '&expand=lesson,image', {
            method:  'GET',
            headers: new Headers({'Authorization': sessionStorage.getItem('jwt')})
        }))
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(viewIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((view) => {
                dispatch(viewFetchDataSuccess(view))
            })
            .catch(() => dispatch(viewHasErrored(true)));
    }
}