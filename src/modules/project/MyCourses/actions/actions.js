export function itemsHasErrored(bool) {
    return {
        type:       'COURSES_ITEMS_HAS_ERRORED',
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type:      'COURSES_ITEMS_IS_LOADING',
        isLoading: bool
    };
}

export function itemsFetchDataSuccess(items) {
    return {
        type: 'COURSES_ITEMS_FETCH_DATA_SUCCESS',
        items
    };
}

export function viewHasErrored(bool) {
    return {
        type:       'COURSES_VIEW_HAS_ERRORED',
        viewHasErrored: bool
    };
}

export function viewIsLoading(bool) {
    return {
        type:      'COURSES_VIEW_IS_LOADING',
        viewIsLoading: bool
    };
}


export function viewFetchDataSuccess(view) {
    return {
        type: 'COURSES_VIEW_FETCH_DATA_SUCCESS',
        view: view
    };
}

export function itemsFetchData(url) {
    return dispatch => {
        dispatch(itemsIsLoading(true));

        fetch(new Request(url + '/course/course-user-link?expand=course,image,passed', {
            method:  'GET',
            headers: new Headers({'Authorization': sessionStorage.getItem('jwt')}),
            param:   {expand: "course"}
        }))
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(itemsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((items) => {
                dispatch(itemsFetchDataSuccess(items))
            })
            .catch(() => dispatch(itemsHasErrored(true)));
    }
}

export function viewFetchData(code, url) {
    return dispatch => {
        dispatch(viewIsLoading(true));

        fetch(new Request(url + '/course/course-user-link/view?code='
            + code + '&expand=course,course.blocks.lessons.lessonCurrentUserLink,image,passed', {
            method:  'GET',
            headers: new Headers({'Authorization': sessionStorage.getItem('jwt')})
        }))
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(viewIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((view) => {
                dispatch(viewFetchDataSuccess(view))
            })
            .catch(() => dispatch(viewHasErrored(true)));
    }
}