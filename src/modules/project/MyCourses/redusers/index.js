import {combineReducers} from 'redux';
import {items, itemsHasErrored, itemsIsLoading, view, viewHasErrored, viewIsLoading} from './redusers';
import {view as lessonView, viewHasErrored as lessonViewHasErrored, viewIsLoading as lessonViewIsLoading} from './redusersLesson.js';

export default combineReducers({
    items,
    itemsHasErrored,
    itemsIsLoading,
    view,
    viewHasErrored,
    viewIsLoading,
    lessonView,
    lessonViewHasErrored,
    lessonViewIsLoading
});