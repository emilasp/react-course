export function itemsHasErrored(state = false, action) {
    switch (action.type) {
        case 'COURSES_ITEMS_HAS_ERRORED':
            return action.hasErrored;

        default:
            return state;
    }
}

export function itemsIsLoading(state = false, action) {
    switch (action.type) {
        case 'COURSES_ITEMS_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function items(state = null, action) {
    switch (action.type) {
        case 'COURSES_ITEMS_FETCH_DATA_SUCCESS':
            return action.items;

        default:
            return state;
    }
}

export function viewHasErrored(state = false, action) {
    switch (action.type) {
        case 'COURSES_VIEW_HAS_ERRORED':
            return action.viewHasErrored;

        default:
            return state;
    }
}

export function viewIsLoading(state = false, action) {
    switch (action.type) {
        case 'COURSES_VIEW_IS_LOADING':
            return action.viewIsLoading;

        default:
            return state;
    }
}

export function view(state = null, action) {
    switch (action.type) {
        case 'COURSES_VIEW_FETCH_DATA_SUCCESS':
            return action.view;

        default:
            return state;
    }
}