export function viewHasErrored(state = false, action) {
    switch (action.type) {
        case 'COURSES_LESSON_VIEW_HAS_ERRORED':
            return action.viewHasErrored;

        default:
            return state;
    }
}

export function viewIsLoading(state = false, action) {
    switch (action.type) {
        case 'COURSES_LESSON_VIEW_IS_LOADING':
            return action.viewIsLoading;

        default:
            return state;
    }
}

export function view(state = null, action) {
    switch (action.type) {
        case 'COURSES_LESSON_VIEW_FETCH_DATA_SUCCESS':
            return action.view;

        default:
            return state;
    }
}