import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {connect} from "react-redux";
import {itemsFetchData} from "../../actions/actions";

import {
    Row,
    Col,
    Card,
    Progress,
    CardBody,
    CardHeader,
    Button,
} from 'reactstrap';

import CourseRow from "../CourseRow";
import LoaderDiv from "../../../../base/components/theme/LoaderDiv";


class MyCourses extends Component {
    items = []

    static contextTypes = {
        env: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchData(this.context.env.API_URL);
    }

    getViewMyCourses () {
        if (this.props.items === null) {
            return <LoaderDiv />;
        }
        if (!this.props.items.length) {
            return <div />;
        }

        return this.props.items.map((item) => (
            <CourseRow key={item.id} item={item}/>
        ))
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col lg="6" sm="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"/><strong>Мои курсы</strong>
                            </CardHeader>
                            <CardBody>
                                {this.getViewMyCourses()}
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="6" sm="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"/><strong>Завершённые курсы</strong>
                            </CardHeader>
                            <CardBody>

                            </CardBody>
                        </Card>
                    </Col>

                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items:      state.course.items,
        hasErrored: state.course.itemsHasErrored,
        isLoading:  state.course.itemsIsLoading
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(itemsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyCourses);
