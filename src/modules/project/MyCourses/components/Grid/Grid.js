import React, {Component} from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';


const styles = {
    root:     {
        display:        'flex',
        flexWrap:       'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
        width:     500,
        height:    450,
        overflowY: 'auto',
    },
};


class Grid extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const parent = this

        return (
            <MuiThemeProvider>
                <div style={styles.root}>
                    <GridList
                        cellHeight={180}
                        style={styles.gridList}
                        cols={3}
                    >
                        <Subheader>December</Subheader>

                        {[1,2,3,4,5,6,7,8,9,12,13,14,15,11].map((items) => (
                            parent.props.items.map((item) => (
                                <GridTile
                                    key={item.course.id}
                                    title={item.course.name}
                                    subtitle={<span>by <b>{item.course.created_by}</b></span>}
                                    actionIcon={<IconButton><StarBorder color="white"/></IconButton>}
                                >
                                    <img src={`/img${item.image}`}/>
                                </GridTile>
                            ))
                        ))}
                        </GridList>
                </div>
            </MuiThemeProvider>
        )
    }
}

export default Grid;
