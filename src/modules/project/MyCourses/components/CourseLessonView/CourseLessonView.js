import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {connect} from "react-redux";
import {viewFetchData} from "../../actions/actionsLesson";
import Comments from "../../../../social/modules/Comments/components/Comments/";

import {
    Row,
    Col,
    Card,
    Progress,
    CardBody,
    CardHeader,
    Button,
} from 'reactstrap';

import ImageLoader from "../../../../base/components/theme/ImageLoader";
import LoaderDiv from "../../../../base/components/theme/LoaderDiv";
import Media from "../../../../base/components/Media";


class CourseLessonView extends Component {
    static contextTypes = {
        env: PropTypes.object.isRequired
    };

    state = {
        openDescription: false
    }

    componentDidMount() {
        this.props.fetchData(this.props.match.params.code, this.context.env.API_URL);
    }

    getMedia = (attribute) => {
        const media  = this.props.view.lesson[attribute]
        const type   = media['type']
        const url    = media['url']
        const props  = media['props'] ? media['props'] : null
        const config = media['config'] ? media['config'] : null

        if (type) {
            return <Media type={type} url={url} props={props} config={config}/>
        }
        return null;
    }

    getView() {
        if (!this.props.view) {
            return <LoaderDiv/>;
        }

        return <Card className="card-accent-primary">
            <CardHeader>
                <h3>{this.props.view.lesson.name}</h3>
            </CardHeader>
            <CardBody>

                <div className="clearfix">
                    <ImageLoader
                        className="rounded float-left"
                        height="200px"
                        src={'/img' + this.props.view.image}
                    />
                    <p dangerouslySetInnerHTML={{__html: this.props.view.lesson.entry}}/>
                </div>

                <div>{this.getMedia('entry_media')}</div>

                <hr/>

                <p dangerouslySetInnerHTML={{__html: this.props.view.lesson.content}}/>

                <div>{this.getMedia('content_media')}</div>

                <hr/>

                <p dangerouslySetInnerHTML={{__html: this.props.view.lesson.conclusion}}/>

                <div>{this.getMedia('conclusion_media')}</div>

                <hr />

                <h2>Обсуждение</h2>
                <Comments
                    object="emilasp\course\common\models\CourseLesson"
                    id={this.props.view.lesson.id}
                />

            </CardBody>
        </Card>


    }

    render() {
        return (
            <div className="animated fadeIn">
                {this.getView()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        view:       state.course.lessonView,
        hasErrored: state.course.lessonViewHasErrored,
        isLoading:  state.course.lessonViewIsLoading
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (code, url) => dispatch(viewFetchData(code, url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseLessonView);
