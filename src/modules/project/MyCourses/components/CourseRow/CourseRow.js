import React, {Component} from 'react';

import {
    Row,
    Col,
    Card,
    Progress,
    CardBody,
    CardHeader,
    CardFooter,
    Button,
} from 'reactstrap';

import {Link} from 'react-router-dom';
import Moment from 'moment';
import ImageLoader from "../../../../base/components/theme/ImageLoader";

class CourseRow extends Component {
    state = {
        active: false
    };

    constructor(props) {
        super(props);

        this.state.close   = this.props.item.status === 0;
        this.state.active  = this.props.item.status === 1;
        this.state.end     = this.props.item.status === 2;
        this.state.waitPay = this.props.item.status === 3;

        Moment.locale('ru');
        this.state.start_at = Moment(this.props.item.start_at).format('d MMM YYYY');
    }

    render() {
        return (
            <div>
                <CardBody className="card card-body">
                    <h3>{this.props.item.course.name}</h3>

                    <div>
                        <ImageLoader
                            className="rounded float-left"
                            height="200px"
                            src={'/img' + this.props.item.image}
                        />

                      {/*  <img src={'/img' + this.props.item.image} className="rounded float-left" height="200px"/>*/}
                        <span dangerouslySetInnerHTML={{__html: this.props.item.course.description}}/>
                    </div>
                    <div className="mb-2">
                        <Progress multi color="">
                            <Progress bar color="success" value={this.props.item.passed} min={0} max={100}>
                                {this.props.item.passed} %
                            </Progress>
                        </Progress>
                    </div>
                    <div className="text-right">
                        <div className="pull-left">
                            <time className="text-muted">{this.state.start_at}</time>
                        </div>
                        <div className="pull-right">
                            <Link to={'/course/' + this.props.item.course.code} className="btn btn-primary">
                                Продолжить обучение
                            </Link>
                        </div>
                    </div>
                </CardBody>
            </div>
        )
    }
}

export default CourseRow;
