import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {connect} from "react-redux";
import {viewFetchData} from "../../actions/actions";

import {
    Row,
    Col,
    Card,
    Progress,
    CardBody,
    CardHeader,
    Button,
} from 'reactstrap';

import LoaderDiv from "../../../../base/components/theme/LoaderDiv";
import CourseBlock from "./components/CourseBlock";
import ImageLoader from "../../../../base/components/theme/ImageLoader";


require("./style.css");

class CourseView extends Component {
    static contextTypes = {
        env: PropTypes.object.isRequired
    };

    state = {
        openDescription: false
    }

    toggleDescription = () => {
        this.setState({openDescription: !this.state.openDescription})
    }

    componentDidMount() {
        this.props.fetchData(this.props.match.params.code, this.context.env.API_URL);
    }

    getView() {
        if (!this.props.view) {
            return <LoaderDiv/>;
        }

        return  <Card className="card-accent-primary">
            <CardHeader>
                <h3>{this.props.view.course.name}</h3>
            </CardHeader>
            <CardBody>

                <p
                    className={this.state.openDescription ? 'description-open' : 'description-close'}
                    onClick={this.toggleDescription}
                >
                    <ImageLoader
                        className="rounded float-left"
                        height="200px"
                        src={'/img' + this.props.view.image}
                    />
                    <span dangerouslySetInnerHTML={{__html: this.props.view.course.description}}/>
                </p>
                <div className="mb-2">
                    <Progress multi color="">
                        <Progress bar color="success" value={this.props.view.passed} min={0} max={100}>
                            {this.props.view.passed} %
                        </Progress>
                    </Progress>
                </div>

                <hr/>

                {this.props.view.course.blocks.map((block) => <CourseBlock key={block.id} item={block} />)}

            </CardBody>
        </Card>
    }

    render() {
        return (
            <div className="animated fadeIn">
                {this.getView()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        view:       state.course.view,
        hasErrored: state.course.viewHasErrored,
        isLoading:  state.course.viewIsLoading
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (code, url) => dispatch(viewFetchData(code, url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseView);
