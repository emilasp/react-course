import React, {Component} from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Badge,
    Table,
    Button
} from 'reactstrap';
import CourseLesson from "./CourseLesson";


class CourseBlock extends Component {
    state = {
        ended: false,
        open:  true,
    }

    constructor(props) {
        super(props);

        this.setOpenStatus()
    }

    toggle = () => this.setState({open: !this.state.open})

    setOpenStatus = () => {
        let open = false;
        let end  = true;

        this.props.item.lessons.map((lesson) => {
            if (lesson.lessonCurrentUserLink && lesson.lessonCurrentUserLink.status === 1) {
                open = true
            }

            if (!lesson.lessonCurrentUserLink || lesson.lessonCurrentUserLink.status === 1) {
                end = false
            }
        })

        this.state.open = open;
        this.state.ended  = end;
    }

    render() {
        return (
            <div className="course-timeline-block-container">

                <div
                    className={`course-timeline-block ${this.state.ended ? 'bg-success' : ''}`}
                    onClick={this.toggle}
                >
                    <strong>{this.props.item.name}</strong>

                    <div className="card-actions">
                        <i className={`fa fa-${this.state.open ? 'minus-square' : 'plus -square'}`}/>
                    </div>

                </div>
                <div className={!this.state.open ? 'd-none' : ''}>

                    <ul className="timeline">

                        {this.props.item.lessons.map((lesson) => <CourseLesson key={lesson.id} item={lesson}/>)}

                    </ul>

                </div>

            </div>
        )
    }
}

export default CourseBlock;
