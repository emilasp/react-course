import React, {Component} from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Badge,
    Table,
    Button
} from 'reactstrap';
import Link from "react-router-dom/es/Link";

class CourseLesson extends Component {
    state = {
        status: 0,
    }

    componentDidMount() {
        this.setStatusClass()
        this.setScore()
    }

    setScore = () => {
        let score = ''
        if (this.props.item.lessonCurrentUserLink) {
            score = this.props.item.lessonCurrentUserLink.score
        }
        this.setState({score: score})
    }

    setStatusClass = () => {
        let className = 'step-empty';

        const status = this.props.item.lessonCurrentUserLink ? this.props.item.lessonCurrentUserLink.status : 0

        switch (status) {
            case 0:
                className = 'step-empty'
                break;
            case 1:
                className = 'step-work'
                break;
            case 2:
                className = 'step-end'
                break;
        }
        this.setState({status: status, statusClass:className})
    }

    getScoreBlock = () => {
        if (this.state.score) {
            return <p className="timeline-lesson-meta">{this.state.score}</p>
        }
        return ' '
    }

    renderRow = () => {
        if (!this.state.status) {
            return <div>
                <div className="timeline-date">
                    {this.getScoreBlock()}
                </div>
                <div className={`timeline-content ${this.state.statusClass}`}>
                    <h3>{this.props.item.name}</h3>
                    <p dangerouslySetInnerHTML={{__html: this.props.item.entry}}/>
                </div>
            </div>
        }

        return <Link to={'/lesson/' + this.props.item.code}>
            <div className="timeline-date">
                {this.getScoreBlock()}
            </div>
            <div className={`timeline-content ${this.state.statusClass}`}>
                <h3>{this.props.item.name}</h3>
                <p dangerouslySetInnerHTML={{__html: this.props.item.entry}}/>
            </div>
        </Link>
    }

    render() {
        return (
            <li>{this.renderRow()}</li>
        )
    }
}

export default CourseLesson;
