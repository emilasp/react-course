import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {connect} from "react-redux";
import {createFetchData} from "../../../../actions/actionsCommentCreate";

import {
    Row,
    Col,
    Input,
} from 'reactstrap';

import Button from './../../../../../base/components/theme/Button';

class CommentsForm extends Component {
    state = {
        fileInputs: [],
        text:       '',
        parent:     '',
    }

    static contextTypes = {
        env: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
    }

    onClick = (event) => {
        this.props.sendForm(
            this.context.env.API_URL,
            this.refs.form,
            this.props.object,
            this.props.id
        )
    }

    onChangeParent = (event) => {
        this.setState({parent: event.target.value})
    }
    onChangeText   = (event) => {
        this.setState({text: event.target.value})
    }


    deleteFile = (index, event) => {
        let fileInputs = this.state.fileInputs;
        delete fileInputs[index];

        this.setState({fileInputs: fileInputs})
    }

    getFileInputsHtml = () => {
        return this.state.fileInputs.map((item, index) => {
                const key = 'file' + index
                return <div className="" key={key} ref={key}>
                    <Row>
                        <Col sm="10"><Input type="file" name="file[]" className="btn btn-sm" /></Col>
                        <Col sm="2" className="comment-form-file-remove">
                            <i className="fa fa-minus" onClick={this.deleteFile.bind(null, index)}/>
                        </Col>
                    </Row>
                </div>
            }
        )
    }

    addFileHandler = (event) => {
        let fileInputs = this.state.fileInputs;
        fileInputs.push('item')
        this.setState({fileInputs: fileInputs})
    }

    componentWillReceiveProps(nextProps) {
        this.props.onSubmit()

        if (!this.state.commentCreateHasErrored) {
            this.setState({fileInputs: [], text: '', parent: ''})
        }
    }

    render() {
        return (
            <div className="comments-list">
                <form ref="form">
                    <Input
                        name="parent"
                        type="text"
                        ref="parent"
                        placeholder="parent"
                        onChange={this.onChangeParent}
                        value={this.state.parent}
                        className="form-control"
                        disabled={this.props.commentCreateIsLoading}
                    />

                    <Input
                        name="text"
                        type="textarea"
                        ref="text"
                        placeholder="text"
                        onChange={this.onChangeText}
                        value={this.state.text}
                        className="form-control"
                        disabled={this.props.commentCreateIsLoading}
                    />

                    <Button onClick={this.addFileHandler} color="primary" className="btn-sm">
                        <i className="fa fa-plus"/> добавить файл
                    </Button>

                    <div>{this.getFileInputsHtml()}</div>

                </form>

                <div className="text-right">
                    <Button
                        onClick={this.onClick}
                        color="success"
                        loading={this.props.commentCreateIsLoading}
                    >
                        <i className="fa fa-send"/> Отправить
                    </Button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        comment:                 state.social.comment,
        commentCreateIsLoading:  state.social.commentCreateIsLoading,
        commentCreateHasErrored: state.social.commentCreateHasErrored,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendForm: (url, form, object, id) => dispatch(createFetchData(url, form, object, id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsForm);
