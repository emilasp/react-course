import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {connect} from "react-redux";
import {commentsFetchData} from "../../../../actions/actionsCommentList";
import CommentsForm from "../CommentsForm/";

import {
    Row,
    Col,
    Card,
    Progress,
    CardBody,
    CardHeader,
    Button,
} from 'reactstrap';

import LoaderDiv from "../../../../../base/components/theme/LoaderDiv";
import ImageLoader from "../../../../../base/components/theme/ImageLoader";

require("./style.css");

class Comments extends Component {
    items = []

    static contextTypes = {
        env: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchData(this.context.env.API_URL, this.props.object, this.props.id);
    }

    /* componentWillReceiveProps(nextProps) {
         this.props.fetchData(this.context.env.API_URL, this.props.object, this.props.id);
     }*/

    onSubmit = (event) => {
        this.props.fetchData(this.context.env.API_URL, this.props.object, this.props.id);
    }

    getView() {
        if (this.props.items === null) {
            return <LoaderDiv/>;
        }
        if (!this.props.items.length) {
            return <div>Комментариев пока нет</div>;
        }

        return this.props.items.map((item) => {
                return <div className="comment my-3 row" key={item.id}>
                    <div className="comment-avatar col-md-1 col-sm-2 text-center pr-1">
                        <a href="">
                            <ImageLoader
                                src={item.createdBy.profile.avatar}
                                height="150px"
                                className="mx-auto rounded-circle img-fluid"
                            />
                        </a>
                    </div>
                    <div className="comment-content col-md-11 col-sm-10">
                        <h6 className="small comment-meta">
                            <a href="#">{item.createdBy.profile.name} </a>
                            {item.created_at}
                        </h6>
                        <div className="comment-body">
                            <p>{item.text}</p>
                            <br/>
                            <div>{item.files.map((item, index) => <a key={index} href={item.url}>{item.name} </a>)}</div>
                            <a href="" className="text-right small"><i className="ion-reply"/> Reply</a>
                        </div>
                    </div>
                </div>
            }
        )
    }

    render() {
        return (
            <div className="comments">
                <div className="comments-list">
                    {this.getView()}
                </div>
                <div className="comments-form">
                    <CommentsForm id={this.props.id} object={this.props.object} onSubmit={this.onSubmit}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log('Comments state', state)
    return {
        items: state.social.comments,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url, object, id) => dispatch(commentsFetchData(url, object, id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Comments);
