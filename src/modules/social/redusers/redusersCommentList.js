export function commentsHasErrored(state = false, action) {
    switch (action.type) {
        case 'COMMENT_LIST_HAS_ERRORED':
            return action.commentsHasErrored;

        default:
            return state;
    }
}

export function commentsIsLoading(state = false, action) {
    switch (action.type) {
        case 'COMMENT_LIST_IS_LOADING':
            return action.commentsIsLoading;

        default:
            return state;
    }
}

export function comments(state = [], action) {
    switch (action.type) {
        case 'COMMENT_LIST_FETCH_DATA_SUCCESS':
            return action.comments;

        default:
            return state;
    }
}