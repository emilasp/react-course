export function commentCreateHasErrored(state = false, action) {
    switch (action.type) {
        case 'COMMENT_CREATE_HAS_ERRORED':
            return action.commentHasErrored;

        default:
            return state;
    }
}

export function commentCreateIsLoading(state = false, action) {
    switch (action.type) {
        case 'COMMENT_CREATE_IS_LOADING':
            return action.commentIsLoading;

        default:
            return state;
    }
}

export function commentCreate(state = [], action) {
    switch (action.type) {
        case 'COMMENT_CREATE_FETCH_DATA_SUCCESS':
            return action.comment;

        default:
            return state;
    }
}