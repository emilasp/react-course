import {combineReducers} from 'redux';
import {commentCreate, commentCreateHasErrored, commentCreateIsLoading} from './redusersCommentCreate';
import {comments, commentsHasErrored, commentsIsLoading} from './redusersCommentList';

export default combineReducers({
    commentCreate,
    commentCreateHasErrored,
    commentCreateIsLoading,
    comments,
    commentsHasErrored,
    commentsIsLoading
});