export function commentsHasErrored(bool) {
    return {
        type:           'COMMENT_LIST_HAS_ERRORED',
        commentsHasErrored: bool
    };
}

export function commentsIsLoading(bool) {
    return {
        type:          'COMMENT_LIST_IS_LOADING',
        commentsIsLoading: bool
    };
}


export function commentsFetchDataSuccess(comments) {
    return {
        type: 'COMMENT_LIST_FETCH_DATA_SUCCESS',
        comments: comments
    };
}

export function commentsFetchData(url, object, id) {
    return dispatch => {
        dispatch(commentsIsLoading(true));

        fetch(new Request(url + '/social/comment/list?object=' + object + '&id=' + id + '&expand=files,createdBy.profile.avatar', {
            method:  'GET',
            headers: new Headers({'Authorization': sessionStorage.getItem('jwt')})
        }))
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(commentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((view) => {
                dispatch(commentsFetchDataSuccess(view))
            })
            .catch(() => dispatch(commentsHasErrored(true)));
    }
}