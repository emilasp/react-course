import axios from "axios/index";

export function createHasErrored(bool) {
    return {
        type:           'COMMENT_CREATE_HAS_ERRORED',
        commentHasErrored: bool
    };
}

export function createIsLoading(bool) {
    return {
        type:          'COMMENT_CREATE_IS_LOADING',
        commentIsLoading: bool
    };
}


export function createFetchDataSuccess(comment) {
    return {
        type: 'COMMENT_CREATE_FETCH_DATA_SUCCESS',
        comment: comment
    };
}

export function createFetchData(url, form, object, object_id) {
    let data = new FormData(form)

    data.append('object', object)
    data.append('object_id', object_id)

    return dispatch => {
        dispatch(createIsLoading(true));

        axios.post(url + '/social/comment/create', data, {
            headers: {
                'Content-Type':  'multipart/form-data',
                'Authorization': sessionStorage.getItem('jwt')
            }
        }).then(result => {

            if (result.data.status === 1) {
                dispatch(createFetchDataSuccess(result.data))
            } else {
                dispatch(createHasErrored(true))
            }

            dispatch(createIsLoading(false));

        }).catch(error => {
            console.log('Load: Catch', error);
            dispatch(createHasErrored(true))
            return error;
        });


        /*fetch(new Request(url + '/social/comment/list?object=' + object + '&id=' + id + '&expand=lesson,image', {
            method:  'GET',
            headers: new Headers({'Authorization': sessionStorage.getItem('jwt')})
        }))
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((view) => {
                dispatch(createFetchDataSuccess(view))
            })
            .catch(() => dispatch(createHasErrored(true)));*/
    }
}