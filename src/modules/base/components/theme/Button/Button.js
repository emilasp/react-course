import React, {Component, FindDOMNode} from 'react';
import {Button as ButtonStrap} from 'reactstrap';


class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text:        this.props.children,
            loading:     !!this.props.loading,
            loadingText: <i className="fa fa-spinner fa-spin"/>,
            style:       {display: 'inline-block'},
        };
    }

    componentDidMount() {
        this.setState({style: {display: 'inline-block', width: this.node.offsetWidth + 'px'}})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({loading: nextProps.loading})
    }

    getWidth = () => {

        return 0
    }

    render() {
        return (
            <div ref={node => this.node = node} style={this.state.style}>
                <ButtonStrap  {...this.props} loading="" disabled={this.state.loading} style={{width: '100%'}}>
                    {this.state.loading ? this.state.loadingText : this.state.text}
                </ButtonStrap>
            </div>
        );
    }
}

export default Button;