import React, {Component} from 'react';
import axios from 'axios';
import {connect} from "react-redux";
import {itemsFetchData} from "../../../Users/Mnagement/actions/actions";


require("./style.css");


class ImageUploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: this.props.src,
            preview: "Выберите аватар",
            uploadUrl: ''
        };

        this._handleSelectImage = this._handleSelectImage.bind(this)
    }

    _handleImageChange(e) {
        if (e.target.files[0]) {
            let reader = new FileReader();
            let file = e.target.files[0];

            reader.onloadend = () => {
                this.setState({
                    file: file,
                    imagePreviewUrl: reader.result
                });
                
                if (this.state.uploadUrl) {
                    /* const request  = new Request(this.state.uploadUrl, {
                         method:  'POST',
                         headers: new Headers(headers),
                         body: JSON.stringify(this.state.attributes)
                     });*/
                }
            }

            reader.readAsDataURL(file)
        }
    }

    _handleSelectImage() {
        this.refs.fileInput.click();
    }

    render() {
        let $imagePreview = null;
        if (this.state.imagePreviewUrl) {
            $imagePreview = (<img src={this.state.imagePreviewUrl}/>);
        } else {
            $imagePreview = <div className="previewText">Выберите аватар</div>;
        }

        return (
            <div className="previewComponent">
                <div className="imgPreview" onClick={this._handleSelectImage}>
                    {$imagePreview}
                </div>
                <input
                    name={this.name}
                    className="fileInput-hidden"
                       ref="fileInput"
                       type="file"
                       onChange={(e) => this._handleImageChange(e)}
                />
            </div>
        )
    }
}

export default ImageUploader;
