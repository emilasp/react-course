import React, {Component} from 'react';
import Chip from 'material-ui/Chip';
import {green50} from 'material-ui/styles/colors';

class ListBoxSelectedItem extends Component {
    constructor(props) {
        super(props);
        this.state = this.props.item;

        this.onDelete = this.onDelete.bind(this);

        this.styles = {
            chip: {
                margin: 1,
            },
            wrapper: {
                display: 'inline-block',
                flexWrap: 'wrap',
            },
        };
    }

    onDelete(event) {
        this.setState({
            isChecked: !this.state.isChecked,
        });

        this.props.onDelete(this);
    }

    render() {
        return (
            <Chip
                backgroundColor={green50}
                key={this.props.item.id}
                onRequestDelete={() => this.onDelete()}
                style={this.styles.chip}
            >
                {this.props.item.name}
            </Chip>
        );
    }
}

export default ListBoxSelectedItem;