import React, {Component} from 'react';

class LoaderDiv extends Component {
    state = {loading: true}


    render() {
        return (
            <div className="text-center"><i className="fa fa-spinner fa-spin fa-5x"/></div>
        );
    }
}

export default LoaderDiv;