import React, {Component} from 'react';


require("./style.css");

const Status = {
    error:   0,
    pending: 1,
    loading: 2,
    load:    3,
}


class ImageLoader extends Component {
    static defaultProps = {
        loadingsrc: '/img/system/loadingImage.gif',
        errorsrc: '/img/system/noImage.jpg',
        /*defaultProps: {width: "100px", height:"100px"}*/
    };

    state = {
        status: Status.pending,
        src:    this.props.loadingsrc,
    }

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.initialize()
    }

    initialize = () => {
        this.onLoading()

        let image     = new Image()
        image.onload  = this.onLoad
        image.onerror = this.onError
        image.src     = this.props.src
    }

    onLoading = (event) => {
        this.props.onLoading && this.props.onLoading(event)
    }

    onLoad = (event) => {
        this.setState({src: this.props.src})

        this.props.onLoad && this.props.onLoad(event)
    }

    onError = (event) => {
        this.setState({src: this.props.errorsrc})

        this.props.onError && this.props.onError(event)
    }

    render() {
        return <img {...this.props} src={this.state.src} />
    }
}

export default ImageLoader;