import React, {Component} from 'react';
import {Container, Row, Col, Button, Input, InputGroupAddon, InputGroup, InputGroupText} from 'reactstrap';

class ErrorPage extends Component {
    getMessage = (message) => {
        return message.replace(/\r\n|\r|\n/g, "<br />");
    }

    render() {
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="12">
                            <div className="clearfix">
                                <h1 className="display-3 mr-4">Error</h1>

                                <h4 className="pt-3" dangerouslySetInnerHTML={{__html: this.props.error.message}}/>
                                <h2>Stack:</h2>
                                <div className="error-stack-trace text-danger"
                                     dangerouslySetInnerHTML={{__html: this.getMessage(this.props.error.stack)}}/>

                                <br />
                                <h2>Info:</h2>
                                <div className="error-stack-trace text-muted"
                                     dangerouslySetInnerHTML={{__html: this.getMessage(this.props.info.componentStack)}}/>
                            </div>


                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default ErrorPage;
