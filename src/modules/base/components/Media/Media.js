import React, {Component} from 'react';
import ReactPlayer from 'react-player'

class Media extends Component {
    constructor(props) {
        super(props);
        this.state = {
            props: this.props.props ? this.props.props : {},
            config: this.props.config ? this.props.config :  {},
        };
    }

    render() {
       return <ReactPlayer {...this.props} {...this.state.props} config={this.state.config} />
    }
}

export default Media;