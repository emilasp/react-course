import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ErrorPage from '../ErrorPage/';

class Application extends Component {
    state = {
        hasError: false,
        error: null,
        info: null,
    }
    static childContextTypes = {
        env: PropTypes.object
    };

    getChildContext() {
        return {
            env:this.props.env
        };
    }

    componentDidCatch(error, info) {
        this.setState({
            hasError: true,
            error: error,
            info: info,
        });
    }

    render() {
        if (this.state.hasError) {
            return (
                <ErrorPage error={this.state.error} info={this.state.info}/>
            );
        }
        return <div>{this.props.children}</div>;
    }
}

export default Application;