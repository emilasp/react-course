import {combineReducers} from 'redux';
import UsersRedusers from './modules/base/Users/Mnagement/redusers/index';
import UserAuth from './modules/base/redusers/auth';
import langReducer from './modules/base/redusers/lang'
import courseReducer from './modules/project/MyCourses/redusers/index'
import socialReducer from './modules/social/redusers/index'
import userCredentialsReducer from "../../react-empty/src/modules/base/Users/Auth/redusers";


const redusers = combineReducers({
    auth: UserAuth,
    users: UsersRedusers,
    user: userCredentialsReducer,
    lang: langReducer,
    course: courseReducer,
    social: socialReducer,
})

const allRedusers = (state, action) => {
    if (action.type === 'AUTH_LOGOUT_CLEAR') {
        state.user.userCredentials = null
    }

    return redusers(state, action)
}

export default allRedusers