const routes = {
  '/': 'Домой',
  '/dashboard': 'Главная',
  '/user/management': 'Пользователи',
};
export default routes;
