export default {
  items: [
    {
      name: 'Главная',
      url: '/dashboard',
      icon: 'icon-speedometer',
     /* badge: {
        variant: 'info',
        text: 'NEW'
      }*/
    },
      {
          name: 'Мои курсы',
          url: '/mycourses',
          icon: 'icon-star'
      },

      /*{
          name: 'Пользователи',
          url: '/user/management',
          icon: 'icon-user'
      },*/

      /*{
          name: 'Права',
          url: '/right/management',
          icon: 'icon-lock'
      },*/

      {
          name: 'Профиль',
          url: '/user/profile',
          icon: 'icon-lock'
      },

      {
          name: 'Выход',
          url: '/logout',
          icon: 'icon-logout'
      }



  ]
};
